Bedienung

    Aufruf: rel2gpx {optionen} {Relation-Id}
    Parameter und Optionen:

    Relation-Id
        Id der zu bearbeitenden Relation. Siehe auch Option -f
    -i datei
        Lese OSM-Daten aus lokaler Datei. Wenn diese Option nicht angegeben wird, dann werden die Daten von openstreetmap.org geladen
    -x datei
        schreibe eine XML-Datei im osm-Format, welche die Daten aller Objekte der Relation enthält (nur zusammen mit -i). 
    -r typ
        Bearbeite alle Relationen mit type=route und route=typ (nur zusammen mit -i). Mögliche Werte für typ: bicycle, hiking, train.
    -f datei
        Lese Relation-Ids aus der Datei (nur zusammen mit -i). In der Textdatei kann pro Zeile eine Id angegeben werden. Zeilen, die mit "#" beginnen, werden nicht ausgewertet. Auch in den Datenzeilen können Kommentare eingefügt werden.
    -g
        Erzeuge eine GPX-Trackdatei
    -p
        Aktiviere die Ausgabe diverser Plausibilitätsprüfungen
    -s
        Aktiviere Statistikausgaben auf STDOUT
    -w
        Ausgabe von Statistikdaten und Plausibilitätshinweisen in eine HTML-Datei
    -o
        Berücksichtigen der Fahrtrichtung (oneway, forward/backward).

    Der Name der GPX- und HTML-Datei wird aus dem Namen der Relation in der OSM-Datenbank gebildet. Existiert dieser nicht, wird die Relation-Id als Name verwendet. Bei Verwendung der Optionen -f oder -i wird der Dateiname aus dem Namen der Relationsliste bzw. der Osm-Datei gebildet. 
