rel2gpx (0.27-5) UNRELEASED; urgency=medium

  * Bump Standards-Version to 4.4.0, no changes.
  * Update gbp.conf to use --source-only-changes by default.

 -- Bas Couwenberg <sebastic@debian.org>  Sun, 05 Aug 2018 20:56:45 +0200

rel2gpx (0.27-4) unstable; urgency=medium

  * Team upload.
  * Drop autopkgtest to test installability.
    (closes: #905121)
  * Add lintian override for testsuite-autopkgtest-missing.

 -- Bas Couwenberg <sebastic@debian.org>  Wed, 01 Aug 2018 19:06:25 +0200

rel2gpx (0.27-3) unstable; urgency=medium

  * Team upload.
  * Update Vcs-Git URL to use HTTPS.
  * Strip trailing whitespace from control & rules files.
  * Update copyright-format URL to use HTTPS.
  * Update Vcs-* URLs for Salsa.
  * Bump Standards-Version to 4.1.5, no changes.
  * Add autopkgtest to test installability.
  * Use pkg-info.mk variables instead of dpkg-parsechangelog output.
  * Update watch file to use HTTPS.

 -- Bas Couwenberg <sebastic@debian.org>  Sun, 29 Jul 2018 18:26:19 +0200

rel2gpx (0.27-2) unstable; urgency=medium

  * Team upload.
  * Add gbp.conf to use pristine-tar by default.
  * Restructure control file with cme, changes:
    - Update Vcs-* URLs to use HTTPS
    - Change libmath-complex-perl dependency to
      perl (>= 5.15.8) | libmath-complex-perl
  * Update copyright file, changes:
    - Add Upstream-Name field
    - Rename license shortname GPLv3 to GPL-3+
    - Wrap lines in standalone GPL-3+ license paragraph
  * Enable parallel builds.
  * Bump Standards-Version to 3.9.8, changes: license shortname, Vcs-* URLs.

 -- Bas Couwenberg <sebastic@debian.org>  Fri, 06 May 2016 18:02:19 +0200

rel2gpx (0.27-1) unstable; urgency=low

  * New upstream version (all patched incorporated)
  * debian/watch: Adapted to new upstream version, thanks to
    Bart Martens <bartm@debian.org> for the patch
  * debian/examples/get-jakobswege-in-deutschland: New usage
    example
  * New upstream homepage

 -- Andreas Tille <tille@debian.org>  Sun, 20 Oct 2013 21:06:17 +0200

rel2gpx (0.26-1) unstable; urgency=low

  * Initial release (Closes: #723724)

 -- Andreas Tille <tille@debian.org>  Thu, 12 Sep 2013 21:34:03 +0200
